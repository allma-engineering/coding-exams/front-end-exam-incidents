# Allma Coding Exam
This is a coding exam administered for consideration of employment as a front-end engineer @ Allma (https://allma.io). This exam does not include algorithmic challenges or data structure challenges, but focuses on real-world use cases at Allma. An API output file (incidents.json) includes an API response for a set of software outage incidents that have occurred across a company. Your job is to build a lite front-end application that reads this API and displays the output of it in a single page according to a set of use cases.

## Rules
1. You should complete the use cases one at a time and complete as many as possible.
2. You may use any resources to complete the test including Google, Stack Overflow, etc.
3. You may _NOT_ enlist the help of other developers to complete the project; your work should be your own.
4. You may use any JS variant e.g. TypeScript, ES201x, etc.
5. You may use any set of JS runtimes, frameworks, libraries, component libraries, themes, etc. e.g. Node, React, Vue, Angular, Bootstrap, etc.

## Criteria for Passing
1. Your solution should meet/implement the valid use cases as outlined and display them on the page.
2. Your solution should enlist readable and understandable code. Feel free to annotate your code as you would normally.
3. Your solution should include instructions on how to run it.
4. The solution cannot fail on it's design aesthetics, e.g. pixel perfection, colors, styles, etc, however quality solutions in this regard will be viewed positively.

## Use Cases to Implement
1. All incidents from the API should be rendered on a single screen/page. You can render it any way you like, e.g. tabular, cards, etc. Render at minimum the following properties of the incident which are included in the API response.
    1. name
    2. severity (severity.name) if it exists
    3. user (participant) on the incident that has assumed the roleId=1 (Incident Commander) if any exists and their avatar
    4. channelName (bonus: create deep links to Slack using incident.channelId & workspace.teamId)
    5. createdOn (convert to local time for display in browser)
    6. duration (the property on the API is in seconds, ideally show in days, minutes, seconds)
    7. incidentStatusId
2. The total number of open (incidentStatusId !== RESOLVED)
3. The total number of incidents created within the last 30 days
4. The mean time to resolution (mean duration for all incidents that have a RESOLVED state, excluding records in any other state)
5. The set of incidents displayed on the screen is able to be filtered by selecting one or more incidentStatusId values and limiting the result set to matches.
6. The set of incidents displayed on the screen is able to be filtered by a search field with a text match against the name and summary properties within the incident object.

## Optional
1. The solution runs from within a Docker container.
2. The solution contains any tests.
